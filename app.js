const turf = require('turf');
const fs = require('fs').promises;
const express = require('express');
const fetch = require('node-fetch');
const osmtogeojson = require('osmtogeojson');

const app = express();
const port = 3000

const featureCollection = {
    "type": "FeatureCollection",
    "features": []
};

async function loadGeoJSONFiles() {
    const files = await fs.readdir('./data');

    for (let i = 0; i < files.length; i++) {
        const file = await fs.readFile(`./data/${files[i]}`, "utf8");
        const json = JSON.parse(file);
        featureCollection.features.push(...json.features);
    }
}

loadGeoJSONFiles();

app.get('/polygon', async (req, res) => {
    const featureSubset = {
        "type": "FeatureCollection",
        "features": []
    };
    let coords = [];
    if (!Array.isArray(req.query.coord)) {
        coords = [req.query.coord];
    } else {
        coords = req.query.coord;
    }
    for (let i = 0; i < coords.length; i++) {
        coords[i] = JSON.parse(coords[i]);
    }
    console.log(req.query.coord);
    if (coords.length >= 3) {
        coords.push(coords[0]);
        const requestBounds = turf.polygon([coords]);
        if (req.query.overpass) {
            await queryOverpass(requestBounds, featureSubset);
        } else {
            for (let i = 0; i < featureCollection.features.length; i++) {
                try {
                    if (turf.intersect(featureCollection.features[i], requestBounds)) {
                        featureSubset.features.push(featureCollection.features[i]);
                    }
                } catch(e) {
                    console.warn(e);
                }
            }
        }
    } else {
        console.error("3 coord's are required to make a polygon.");
    }
    res.send(`${JSON.stringify(featureSubset)}`);
})

async function queryOverpass(requestBounds, featureSubset) {
    const bbox = turf.bbox(requestBounds);
    const url = `https://overpass.kumi.systems/api/interpreter?data=[out:json];(way["building"](${bbox[1]},${bbox[0]},${bbox[3]},${bbox[2]});relation["building"](${bbox[1]},${bbox[0]},${bbox[3]},${bbox[2]}););out;>;out qt;`;
    console.log(url);
    const result = await fetch(url);
    const overpassJSON = await result.json();
    const geoJSON = osmtogeojson(overpassJSON, { flatProperties: true });
    for (let i = 0; i < geoJSON.features.length; i++) {
        if (turf.intersect(geoJSON.features[i], requestBounds)) {
            featureSubset.features.push(geoJSON.features[i]);
        }
    }
}

app.listen(port, () => console.log(`GeoJSON Building server listening at http://localhost:${port}`))